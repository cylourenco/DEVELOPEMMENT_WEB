<?php

require_once('fonctions.php');
require_once('mariaDB.php');
$db = connexion_bdd();

// Test Bouton Vider Panier
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['panier']) && $_POST['panier'] === 'Vider') {
    // Exécute la fonction pour vider le panier
    vider_panier($db);
}
// Test Bouton Commander (Article)
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['article']) && $_POST['article'] === 'Commander') {
    // Exécute la fonction pour vider le panier
    ajout_panier($db);
}
    

?>