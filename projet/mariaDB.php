<?php

// Fonction de connexion à la base de données
function connexion_bdd(){
    try{
        // Création d'une nouvelle connexion PDO à la base de données
        $db = new PDO("mysql:host=localhost; port=3307; dbname=jeux-videos", "jeux-videos", "IsImA_2023/%");
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $db->exec("SET NAMES 'utf8'"); // pour mettre les accents

        return $db; // Retourne la connexion PDO

    } catch(PDOException $e){
        // En cas d'erreur de connexion, affiche un message d'erreur
        echo "Erreur de connexion : " . $e->getMessage();
        return null;
    }    
}



// Fonction pour sélectionner le contenu du panier
function select_panier($db){
    // Requête pour sélectionner les articles du panier avec leur quantité et prix
    $query = "SELECT pa.id_article, pa.quantite, pa.prix_ttc, a.libelle
    FROM panier_article pa
    JOIN article a ON pa.id_article = a.id";
    $result = $db->query($query);

    return $result; // Retourne le résultat de la requête
}



// Fonction pour obtenir le libellé d'un article en fonction de son ID
function libelle($id_article, $db){
    // Préparation de la requête pour récupérer le libellé de l'article en fonction de son ID
    $libelle_query = $db->prepare("SELECT libelle FROM article WHERE id = :id_article");
    $libelle_query->bindParam(':id_article', $id_article, PDO::PARAM_INT);
    $libelle_query->execute();
    $libelle_result = $libelle_query->fetch(PDO::FETCH_ASSOC);

    $libelle = $libelle_result['libelle']; // Récupération du libellé

    return $libelle; // Retourne le libellé de l'article
}

?>
