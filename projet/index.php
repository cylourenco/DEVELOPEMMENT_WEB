<!DOCTYPE html>
<html lang="fr">

<?php 
    // Inclusion des fichiers de fonctions et de connexion à la base de données
    require_once('fonctions.php');
    require_once('mariaDB.php');
    
    // Connexion à la base de données
    $db = connexion_bdd();
?>

<head>
    <title>Achat de jeux vidéos</title>
    <!-- Icône du site -->
    <link rel="icon" type="image/gif" href=" img/img_site/icone-site.gif">
    <!-- Type de contenu et de l'encodage -->
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta charset="UTF-8">
    <!-- Liaison vers la feuille de style -->
    <link href="style/index.css" rel="stylesheet" />
</head>

<body>
    <div class='page'>
        <div class='titre'>
            <!-- Lien vers la page principale avec une image et un titre -->
            <a href='http://projet/'>
                <img src="img/img_site/icone-site.gif" alt="Icone du site"/>
                <h1>Jeux vidéos</h1>
            </a>
        </div>

        <!-- Section pour le formulaire d'authentification -->
        <div class='authentification'>
            <form>
                <div class='case_bleu'>
                    <label for="email"><p>Adresse email</p></label>
                    <input type="email" name="email" /></br>
                    <label for="password"><p>Mot de passe</p></label>
                    <input type="password" name="password" />
                </div>
            </form>
            <!-- Boutons d'inscription et de connexion -->
            <div class='boutons-container'>
                <div class='bouton'>S'inscrire</div>
                <div class='bouton'>Connexion</div>
            </div>
        </div>

        <!-- Afficher des articles -->
        <div class='contenu'>
            <?php 
            // Appel de la fonction pour gérer les articles et afficher le contenu
            gerer_article($db);
            ?>
        </div>

        <!-- Afficher du panier d'achat -->
        <div class='panier'>
            <div class='sous_panier'>
                <h3><img src="img/img_site/caddie.gif" /> Votre panier</h3>
                <hr>
                <?php
                // Appel de la fonction pour afficher le contenu du panier
                affichage_panier($db);
                ?>
            </div>
        </div>

        <div class='pied_de_page'></div>
    </div>
</body>

</html>
