<?php
session_start();
require('bouton.php');
    
// Fonction pour ajouter un article au panier
function ajout_panier($db) {
    $id_article = $_POST['id'];
    $prix_article = $_POST['prix'];

    // Vérifie si l'article existe déjà dans le panier
    $article_exists_query = $db->prepare("SELECT quantite FROM panier_article WHERE id_panier = 1 AND id_article = :id_article");
    $article_exists_query->bindParam(':id_article', $id_article, PDO::PARAM_INT);
    $article_exists_query->execute();
    $article_exists_result = $article_exists_query->fetch(PDO::FETCH_ASSOC);

    // Si l'article existe, incrémente sa quantité
    if ($article_exists_result) {
        incrementer_quantite_article($db, $id_article);
    } else {
        inserer_article_panier($db, $id_article, $prix_article);
    }

    // Récupère l'ID de la catégorie de l'article pour la redirection
    $id_categorie = obtenir_id_categorie($db, $id_article);
    header("Location: index.php?id_categorie=$id_categorie");
    exit();
}



// Fonction pour incrémenter la quantité d'un article existant dans le panier
function incrementer_quantite_article($db, $id_article) {
    // Sélectionne la quantité actuelle de l'article dans le panier
    $quantite_query = $db->prepare("SELECT quantite FROM panier_article WHERE id_panier = 1 AND id_article = :id_article");
    $quantite_query->bindParam(':id_article', $id_article, PDO::PARAM_INT);
    $quantite_query->execute();
    $quantite_result = $quantite_query->fetch(PDO::FETCH_ASSOC);

    // Incrémente la quantité de l'article dans le panier
    $quantite = $quantite_result['quantite'] + 1;

    // Met à jour la quantité de l'article dans le panier
    $update_query = $db->prepare("UPDATE panier_article SET quantite = :quantite WHERE id_panier = 1 AND id_article = :id_article");
    $update_query->bindParam(':quantite', $quantite, PDO::PARAM_INT);
    $update_query->bindParam(':id_article', $id_article, PDO::PARAM_INT);
    $update_query->execute();
}



// Fonction pour insérer un nouvel article dans le panier
function inserer_article_panier($db, $id_article, $prix_article) {
    // Récupère le taux de TVA de l'article
    $taux_query = $db->prepare("SELECT taux FROM article JOIN tva ON article.id_tva = tva.id WHERE article.id = :id_article");
    $taux_query->bindParam(':id_article', $id_article, PDO::PARAM_INT);
    $taux_query->execute();
    $taux_result = $taux_query->fetch(PDO::FETCH_ASSOC);

    // Calcule le prix hors taxes de l'article
    $taux = $taux_result['taux'];
    $prix_ht = $prix_article - ($taux / 100) * $prix_article;

    // Insère l'article dans le panier avec une quantité initiale de 1
    $insert_query = $db->prepare("INSERT INTO panier_article (id_panier, id_article, quantite, prix_ht, prix_tva, prix_ttc) VALUES (1, :id_article, 1, :prix_ht, :taux, :prix_article)");
    $insert_query->bindParam(':id_article', $id_article, PDO::PARAM_INT);
    $insert_query->bindParam(':prix_ht', $prix_ht, PDO::PARAM_STR);
    $insert_query->bindParam(':taux', $taux, PDO::PARAM_STR);
    $insert_query->bindParam(':prix_article', $prix_article, PDO::PARAM_STR);
    $insert_query->execute();
}



// Fonction pour obtenir l'ID de la catégorie d'un article
function obtenir_id_categorie($db, $id_article) {
    $categorie_query = $db->prepare("SELECT id_categorie FROM article WHERE id = :id_article");
    $categorie_query->bindParam(':id_article', $id_article, PDO::PARAM_INT);
    $categorie_query->execute();
    $categorie_result = $categorie_query->fetch(PDO::FETCH_ASSOC);
    return $categorie_result['id_categorie'];
}



// Fonction pour vider le panier
function vider_panier($db){
    try {
        // Supprime tous les articles du panier
        $delete_query = $db->prepare("DELETE FROM panier_article WHERE panier_article.id_panier = 1");
        $delete_query->execute();
    
        // Redirection vers la page précédente
        header("Location: {$_SERVER['HTTP_REFERER']}");
        exit();
    } catch (PDOException $e) {
        // Gestion des erreurs de la base de données
        echo "Error: " . $e->getMessage();
        exit();
    }
}



// Fonction pour afficher le contenu du panier
function affichage_panier($db){
    // Sélectionne tous les articles du panier
    $result = select_panier($db);

    $prix_total = 0;

    echo "<div class='panier-container'>";

    // Parcourt tous les articles du panier
    while ($data = $result->fetch(PDO::FETCH_ASSOC)) {
        $id_article = $data['id_article'];

        // Récupère le libellé de l'article
        $libelle = libelle($id_article, $db);

        // Calcule le prix total de chaque article dans le panier
        $prix_total_article = $data['quantite'] * $data['prix_ttc'];
        $prix_total += $prix_total_article; // Ajoute le prix total de l'article au total général

        // Affiche chaque article dans le panier avec son libellé, sa quantité et son prix total
        echo "<div class='panier-item'>";
        echo "<span class='nom-jeu'>{$libelle}</span>";
        echo "<span class='prix-quantite'><br>{$data['quantite']} x {$data['prix_ttc']} = {$prix_total_article} € </span>";
        echo "</div>";
    }

    // Affiche le prix total du panier et un bouton pour vider le panier
    if ($prix_total > 0){
        $prix_total = number_format($prix_total, 2);
        echo '<hr>';
        echo "<div class='prix-total' style='text-align: right;'> TOTAL : {$prix_total} € </div>";
        echo '<form class="vider-panier-bouton"  method="post" action="">';
        echo '<button class="bouton" type="submit" name="panier" value="Vider">Vider le panier</button>';
        echo '</form>';
    }
}


// Fonction pour afficher un article individuel
function afficher_article($row) {
    if (isset($row['image'])) { // Vérifie si l'article a une image associée
        $imageURL = "img/img_articles/" . $row['image']; // Construit l'URL de l'image
        echo "<div class='jeu-box'>";
        echo "<img class='jeu-img' src='$imageURL' alt='Image de l'article'><br>"; // Affiche l'image de l'article
        echo "<div class='texte'>";
        echo "<h3>{$row['libelle']}</h3>"; // Affiche le libellé de l'article
        echo "<p>{$row['detail']}...</p>"; // Affiche une partie du détail de l'article
        echo "<p class='prix'>{$row['prix_ttc']}€</p>"; // Affiche le prix de l'article
        echo "<form method='post' action='bouton.php'>";
        echo "<input type='hidden' name='id' value='{$row['id']}'>"; // Champ caché pour l'ID de l'article
        echo "<input type='hidden' name='prix' value='{$row['prix_ttc']}'>"; // Champ caché pour le prix de l'article
        echo "<button class='bouton' type='submit' name='article' value='Commander'>Commander</button>"; // Bouton pour commander l'article
        echo "</form>";
        echo "</div></div>";
    } else {
        echo "Aucune image associée à cet article."; // Message si aucune image associée à l'article
    }
}

// Fonction pour afficher les articles selon une catégorie
function afficher_articles_categorie($stmt) {
    echo "<div class='images-container'>";
    while ($row = $stmt->fetch()) {
        echo "<div class='image-item'>";
        afficher_article($row); // Appelle la fonction pour afficher chaque article
        echo "</div>";
    }
    echo "<div class='pied_de_page'><a href='http://projet/' id='bouton_retour' class='bouton'>Retour</a></div>"; // Bouton retour
    echo "</div>";
}

// Fonction pour afficher les catégories
function afficher_categories($result) {
    while ($data = $result->fetch(PDO::FETCH_ASSOC)) {
        echo "<article class='categorie'>";
        echo "<a href='index.php?id_categorie={$data['id']}' style='order: {$data['ordre_affichage']}'>";
        echo "<div class='cercle'>";
        if ($data['libelle'] == "combat") { // Vérifie si la catégorie est "combat" -> dimensions différentes à l'affichage
            echo '<div class="categorie-image">';
            echo '<img src="img/img_categories/epees.gif">';
            echo '</div>';
        } else {
            echo '<img src="img/img_categories/' . $data['image'] . '" alt=""> '; // Affiche l'image de la catégorie
        }
        echo "</div></a><br>";
        if ($data['libelle'] == "aventure" || $data['libelle'] == "horreur") {
            echo "<p>Jeux<br>d'{$data['libelle']}</p>"; // Affiche le nom de la catégorie (jeux d'...) + besoin d'un saut de ligne
        } elseif (strlen($data['libelle']) <= 5) {
            echo "<p>Jeux de {$data['libelle']}</p>"; // Affiche le nom de la catégorie (jeux de...)
        } else {
            echo "<p>Jeux de<br>{$data['libelle']}</p>"; // Affiche le nom de la catégorie (jeux de...) + besoin d'un saut de ligne
        }
        echo "</article>";
    }
}

// Fonction principale pour gérer les articles
function gerer_article($db) {
    if (isset($_GET['id_categorie'])) {
        $id_categorie = $_GET['id_categorie'];
        $query = "SELECT id, libelle, image, detail, prix_ttc FROM article WHERE id_categorie = :id_categorie";
        $stmt = $db->prepare($query);
        $stmt->bindParam(':id_categorie', $id_categorie, PDO::PARAM_INT);
        $stmt->execute();
        afficher_articles_categorie($stmt); // Affiche les articles de la catégorie spécifiée
    } else {
        $query = "SELECT * FROM categorie ORDER BY ordre_affichage";
        $result = $db->query($query);
        afficher_categories($result); // Affiche les catégories
    }
}


?>