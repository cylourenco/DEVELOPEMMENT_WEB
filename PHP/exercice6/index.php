<!DOCTYPE html>  
<html>

	<head>
		<title>Exercice 6</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<link href="exercice6.css" rel="stylesheet" />
	</head>

	<body>
	<h2>Nombres premiers entre 1 et 10000</h2>
	
	
	<table border="1px">
	<?php
	function estNbPrem($nb)
	{
		if($nb <= 1)
			return true;
		for($i=2; $i<=sqrt($nb); $i++)
		{
			if($nb % $i == 0)
				return false;
		}
		return true;
	}
	echo "<tr>";		
	for($i=1; $i<10000; $i++)
	{
		$color = estNbPrem($i) ? 'red' : 'black';
		echo "<td style='color: $color;'>" . $i . "</td>";
		if ($i % 30 == 0)
		{
			echo "</tr><tr>";
		}
		
	}
	echo "</tr>";
	?>
	</table>	
	</body>

</html>