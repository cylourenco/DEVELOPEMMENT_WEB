<!DOCTYPE html>  
<html>

	<head>
		<title>Exemple 3</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

	</head>

	<body>
		<div>
		
		<h1>
			Enregistrement d'un utilisateur
		</h1>
		<form method="post" action="enregistre_utilisateur.php">
			<label>Nom</label> : <input type="text" name="Nom" />
			<label>Prénom</label> : <input type="text" name="Prénom" />
			</br></br>
			
			<label>Sexe : </label> 
			Homme<input type="radio" name="Sexe" value="Homme" /> 
			Femme<input type="radio" name="Sexe" value="Femme" checked /> 
			</br></br>
			
			<label>Fonctions : </label> 
			<select name="Fonctions" >
			<option value="Enseignant" > Enseignant </option>
			<option value="Elèves" > Elèves </option>
			<option value="Ingénieur" > Ingénieur </option>
			<option value="Retraité" > Retraité </option>
			<option value="Autre" > Autre </option>
			</select >
			</br></br>
			
			<label>Commentaires : </label> </br>
			<textarea name="Commentaires"rows="3"> </textarea>
			</br></br> 
			
			<input type="submit" value="Enregistrer" />

		</form>
	
		</div>
			
	</body>

</html>