# ZZ2-Developpement_web

Ce cours permet de vous initier au développement web en explorant HTML, CSS, PHP et MariaDB. 

## Auteurs

* **Cynthia LOURENCO** _alias_ [@cylourenco](https://gitlab.isima.fr/cylourenco)
* **Louane MECHOUD** _alias_ [@lomechoud](https://gitlab.isima.fr/lomechoud)

